import requests
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

auth_url = "https://www.strava.com/oauth/token"

payload = {
    'client_id': "61035",
    'client_secret': '8a2c4aab365d412d9ca66dba691a6923f327cda0',
    'refresh_token': '01c621199db3d297b821390838957dfc1cf0ecee',
    'grant_type': "refresh_token",
    'f': 'json'
}

print("Requesting Token...\n")
res = requests.post(auth_url, data=payload, verify=False)
access_token = res.json()['access_token']
print("Access Token = {}\n".format(access_token))


url = "https://www.strava.com/api/v3/athlete"

payload={}
headers = {
  'Authorization': 'Bearer ' + access_token
}

response = requests.request("GET", url, headers=headers, data=payload).json()

print(response)

